package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Form;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */

public class PizzaResourceTest extends JerseyTest {
    private PizzaDao dao;
    private IngredientDao daoIngredient;
        
    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        return new ApiV1();
    }

    @Before
    public void setEnvUp() {
        daoIngredient = BDDFactory.buildDao(IngredientDao.class);
        daoIngredient.createTable();
        dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTableAndIngredientAssociation();
    }

    @After
    public void tearEnvDown() throws Exception {
        dao.dropTableAndIngredientAssociation();
        daoIngredient.dropTable();
    }

    @Test
    public void testGetExistingPizza() {

        Pizza pizza = new Pizza();
        pizza.setName("Pizza ehfehhehfheehehfheh");

        long id = dao.insert(pizza.getName());
        pizza.setId(id);

        Response response = target("/pizzas/" + id).request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        System.out.println(result.getName());
        assertEquals(pizza, result);
    }

    @Test
    public void testGetNotExistingPizza() {
    Response response = target("/pizzas/125").request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }

    public void testCreatePizza() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("ehfehehehehfheehehfheh");

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));
    
        // On vérifie le code de status à 201
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    
        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
    
        // On vérifie que le champ d'entête Location correspond à
        // l'URI de la nouvelle entité
        assertEquals(target("/pizzas/" +
            returnedEntity.getId()).getUri(), response.getLocation());
        
        // On vérifie que le nom correspond
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
    }

    @Test
    public void testCreateSamePizza() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("ehfehehehehfheehehfheh");
        dao.insert(pizzaCreateDto.getName());

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

}