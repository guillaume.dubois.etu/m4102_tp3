package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.ArrayList;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;

public class Pizza {

    private long id;
    private String name;
    private List<Ingredient> ingredients;

    public Pizza() {
        this.ingredients = new ArrayList<Ingredient>();
    }

    public Pizza(long id, String name) {
        this.id = id;
        this.name = name;
        this.ingredients = new ArrayList<Ingredient>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredients(Ingredient ingredient) {
        this.ingredients.add(ingredient);
    }

    public static PizzaDto toDto(Pizza p) {
        PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());
        dto.setIngredients(p.getIngredients());
    
        return dto;
    }

    public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());
        pizza.setIngredients(dto.getIngredients());
    
        return pizza;
      }

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", ingredients=" + ingredients + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
    }
    
    public static PizzaCreateDto toCreateDto(Pizza pizza) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
        
        return dto;
    }
    
    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());
    
        return pizza;
    }

}
