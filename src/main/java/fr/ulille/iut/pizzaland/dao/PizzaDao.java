package fr.ulille.iut.pizzaland.dao;
import org.jdbi.v3.sqlobject.transaction.Transaction;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.Ingredient;


public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizza (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza INTEGER NOT NULL, idIngredient INTEGER NOT NULL, CONSTRAINT PizzaIngredientsAssociation_pk PRIMARY KEY (idPizza, idIngredient), FOREIGN KEY (idIngredient) REFERENCES Ingredient(id), FOREIGN KEY (idPizza) REFERENCES pizza(id))")
    void createAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizza")
    void dropPizzaTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();

    @Transaction
    default void createTableAndIngredientAssociation() {
        createPizzaTable();
        createAssociationTable();
    }

    @Transaction
    default void dropTableAndIngredientAssociation() {
        dropAssociationTable();
        dropPizzaTable();
    }

    @Transaction
    default void removePizza(long id) {
        removePizzaFromAssociationTable(id);
        removePizza(id);
    }

    @SqlUpdate("INSERT INTO pizza (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);

    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation VALUES (:pizzaId, :ingredientId)")
    long addIngredient(long pizzaId, long ingredientId);

    @SqlQuery("SELECT * FROM pizza")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT ingredients.* FROM ingredients JOIN PizzaIngredientsAssociation ON idIngredient = ingredients.id WHERE pizzaId = :id")
    @RegisterBeanMapper(Pizza.class)
    List<Ingredient> getIngredients(long id);

    @SqlQuery("SELECT * FROM pizza WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(long id);

    @SqlQuery("SELECT * FROM ingredients WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(String name);

    @SqlUpdate("DELETE FROM pizza WHERE id = :id")
    void removePizzaFromTable(long id);

    @SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE idPizza = :id")
    void removePizzaFromAssociationTable(long id);

    @SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE idPizza = :idPizza AND idIngredient = :idIngredient")
    void removeIngredient(long idPizza, long idIngredient);
}
